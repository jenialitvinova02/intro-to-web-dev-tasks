import "../styles/style.css";
import { createElement } from "./createElement";
import startHeader from "../LandingPage/header/header";
import startFeatHeader from "../FeaturePage/header/header";
import startFeatMain from "../FeaturePage/main/main";
import startSignUpHeader from "../SignUpPage/header/header";
import startSignUpMain from "../SignUpPage/main/main";
import startMain from "../LandingPage/main/main";
import startFooter from "../footer/footer";

const root = document.querySelector("#root");

createElement({ tag: "header", parent: root });
const newHeader = document.querySelector("header");

createElement({ tag: "main", parent: root });
const newMain = document.querySelector("main");

startHeader(newHeader);
startMain(newMain);
startFooter(root);

const { body } = document;

function handleClicklistener(event) {
  if (event.target.id === "buttondiscover") {
    document.querySelector(".root__header").remove();
    document.querySelector(".root__main").remove();
    document.querySelector(".main__image__girl").remove();
    startFeatHeader(newHeader);
    startFeatMain(newMain);
  }

  if (
    event.target.id === "buttonjoin" ||
    event.target.id === "button-JoinNow" ||
    event.target.id === "join-feat"
  ) {
    document.querySelector(".root__header").remove();
    document.querySelector(".root__main").remove();
    document.querySelector(".main__image__girl").remove();
    startSignUpHeader(newHeader);
    startSignUpMain(newMain);
  }

  if (event.target.id === "discover-sign") {
    document.querySelector(".root__header").remove();
    document.querySelector(".root__main").remove();
    document.querySelector(".main__image__girl").remove();
    startHeader(newHeader);
    startMain(newMain);
  }
}

body.addEventListener("click", handleClicklistener);
newMain.addEventListener("click", handleClicklistener);
