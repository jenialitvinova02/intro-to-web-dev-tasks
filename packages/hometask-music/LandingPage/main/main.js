import { createElement } from "../../components/createElement";
import "./main.css";
import girl from "../../assets/images/background-page-landing.png";

export default function startMain(parent) {
  createElement({
    tag: "div",
    attributes: { class: "root__main root__main-startPage" },
    parent,
  });
  const main = document.querySelector(".root__main");

  createElement({
    tag: "h1",
    attributes: { class: "main__title" },
    parent: main,
    content: "Feel the music",
  });
  createElement({
    tag: "p",
    attributes: { class: "main__text" },
    parent: main,
    content: "Stream over 10 million songs with one click",
  });
  createElement({
    tag: "button",
    attributes: { class: "main__button", id: "button-JoinNow" },
    parent: main,
    content: "Join now",
  });

  createElement({
    tag: "img",
    attributes: {
      class: "main__image__girl main__image__girl-startPage",
      src: girl,
    },
    parent,
  });
}
