import { createElement } from "../../components/createElement";
import "./main.css";
import view from "../../assets/images/music-titles.png";

export default function startFeatMain(parent) {
  createElement({
    tag: "div",
    attributes: { class: "root__main root__main-featPage" },
    parent,
  });
  const main = document.querySelector(".root__main");

  createElement({
    tag: "h1",
    attributes: { class: "main__title-featPage" },
    parent: main,
    content: "Discover new music",
  });

  createElement({
    tag: "div",
    attributes: { class: "main__buttons-featPage" },
    parent: main,
  });
  const buttonsFeat = document.querySelector(".main__buttons-featPage");

  createElement({
    tag: "button",
    attributes: { class: "main__button-feat" },
    parent: buttonsFeat,
    content: "Charts",
  });

  createElement({
    tag: "button",
    attributes: { class: "main__button-feat" },
    parent: buttonsFeat,
    content: "Songs",
  });

  createElement({
    tag: "button",
    attributes: { class: "main__button-feat" },
    parent: buttonsFeat,
    content: "Artists",
  });

  createElement({
    tag: "p",
    attributes: { class: "main__text-featPage" },
    parent: main,
    content:
      "By joing you can benefit by listening to the latest albums released",
  });

  createElement({
    tag: "img",
    attributes: {
      class: "main__image__girl main__image__girl-featPage",
      src: view,
    },
    parent,
  });
}
